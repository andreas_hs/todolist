package no.ntnu.idatg1002;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;

/**
 * JavaFX App
 * @author Basically Co.
 * @version 1.0
 */
public class App extends Application {

    /**
     * Sets up a private static scene.
     */
    private static Scene scene;

    static {
        try {
            scene = new Scene(loadFXML("primary"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Start method that starts the app.
     * @param stage the stage of the app.
     */
    @Override
    public void start(Stage stage) {
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Method that sets the root(FXML file).
     * @param fxml which FXML to use.
     * @throws IOException can be caught.
     */
    public static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    /**
     * Sets the root for the scene.
     * @param root root to be set.
     */
  public static void setRoot(Parent root){
        scene.setRoot(root);
    }

    /**
     * Loads chosen FXML file.
     * @param fxml FXML file to be loaded.
     * @return returns the loaded FXML file.
     * @throws IOException can be caught.
     */
    private static Parent loadFXML(String fxml) throws IOException {
        var fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    /**
     * The main method only has the launch method.
     * @param args launch arguments. Not used.
     */
    public static void main(String[] args) {
        launch();
    }

}
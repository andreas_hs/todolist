package no.ntnu.idatg1002;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import no.ntnu.idatg1002.registry.Task;
import no.ntnu.idatg1002.registry.TaskRegistry;

import java.io.IOException;
import java.time.LocalDate;

/**
 * The controller for the Edit task FXML.
 */
public class EditTaskController {

    /**
     * All the fields for the Edit task controller.
     * As well as creating the taskRegistry object.
     */


    @FXML
    private TextField titleTextField;

    @FXML
    private TextField descriptionTextField;

    @FXML
    private ComboBox<?> priorityComboBox;

    @FXML
    private TextField categoryTextField;

    @FXML
    private DatePicker startDatePicker;

    @FXML
    private DatePicker finishDatePicker;

    private Task task;
    boolean startBeforeEnd = false;

    /**
     * Sets the chosen task.
     *
     * @param task task to be set.
     */
    void setEditTask(Task task) {
        this.task = task;
    }

    String priority;

    /**
     * Sets the task with the values from the text fields and add these to the registry.
     *
     * @throws IOException            can be caught.
     */
    @FXML
    private void setTask() throws IOException {
        TaskRegistry.readFromFile();
        String title = titleTextField.getText();
        String description = descriptionTextField.getText();
        String category = categoryTextField.getText();
        priority = (String) priorityComboBox.getValue();
        LocalDate startDate = startDatePicker.getValue();
        LocalDate endDate = finishDatePicker.getValue();


        if (startDatePicker.getValue() != null && finishDatePicker.getValue() != null) {
            startBeforeEnd = startDatePicker.getValue().isBefore(finishDatePicker.getValue());
        } else if (startDatePicker.getValue() == null && finishDatePicker.getValue() != null) {
            startBeforeEnd = true;
        }
        boolean requiredFieldsPresent = (!titleTextField.getText().equals("") && priorityComboBox.getValue() != null && finishDatePicker.getValue() != null);

        if (requiredFieldsPresent && startBeforeEnd) {
            TaskRegistry.addTask(new Task(title, priority, description, category, startDate, endDate));
            try {
                switchToPrimary();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            if (!requiredFieldsPresent) {
                alert.setHeaderText("You missed a required field");
            } else  {
                alert.setHeaderText("End date can not be before start date");
            }
            alert.show();

        }

    }


    /**
     * Simple method that lets the user cancel the edit and return to the main window.
     */
    @FXML
    private void cancelEdit() {
        TaskRegistry.addTask(task);
        try {
            switchToPrimary();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Switches the app to the primary FXML file.
     *
     * @throws IOException can be caught.
     */
    @FXML
    private void switchToPrimary() throws IOException {
        App.setRoot("primary");

    }


    /**
     * Initializes the FXML.
     */
    @FXML
    public void initialize() {
        // Must use run later, since variable is passed after initial load.
        Platform.runLater(() -> {
            if (task != null) {
                titleTextField.setText(task.getTitle());
                descriptionTextField.setText(task.getDescription());
                categoryTextField.setText(task.getCategory());
                int comboBoxIndex = priorityComboBox.getItems().indexOf(task.getPriority());
                priorityComboBox.getSelectionModel().select(comboBoxIndex);
                startDatePicker.setValue(task.getStartDate());
                finishDatePicker.setValue(task.getEndDate());
            }
        });
    }
}

package no.ntnu.idatg1002;

import java.io.IOException;
import java.time.LocalDate;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import no.ntnu.idatg1002.registry.Task;
import no.ntnu.idatg1002.registry.TaskRegistry;

/**
 * Controller for the Add task FXML.
 */
public class AddTaskController {

    /**
     * Creates the registry and a primary controller object so that the controller can be switched.
     * Also declares the labels,text fields, etc.
     */
    PrimaryController primaryController = new PrimaryController();

    @FXML
    private TextField titleTextField;

    @FXML
    private TextField descriptionTextField;

    @FXML
    public Label warningLabel;

    @FXML
    private TextField categoryTextField;

    @FXML
    private ComboBox<String> priorityComboBox;

    @FXML
    private DatePicker startDatePicker;

    @FXML
    private DatePicker endDatePicker;

    boolean startBeforeEnd = false;

    /**
     * This method switched back to the primary controller when the "Add" button is clicked.
     *
     * @throws IOException            can be caught.
     * @throws ClassNotFoundException can be caught.
     */
    @FXML
    private void switchToPrimary() throws IOException, ClassNotFoundException {
        App.setRoot("primary");
    }

    /**
     * This method adds the task with the text taken from the text fields.Then it uses the switchToPrimary to switch controller.
     *
     * @throws ClassNotFoundException can be caught.
     */
    @FXML
    void setTask() throws ClassNotFoundException {
        String title = titleTextField.getText();
        String description = descriptionTextField.getText();
        String category = categoryTextField.getText();
        String priority = priorityComboBox.getValue();
        LocalDate startDate = startDatePicker.getValue();
        LocalDate endDate = endDatePicker.getValue();

        if (startDatePicker.getValue() != null && endDatePicker.getValue() != null) {
            startBeforeEnd = startDatePicker.getValue().isBefore(endDatePicker.getValue());
        } else if (startDatePicker.getValue() == null && endDatePicker.getValue() != null) {
            startBeforeEnd = true;
        }
        boolean requiredFieldsPresent = (!titleTextField.getText().equals("") && priorityComboBox.getValue() != null && endDatePicker.getValue() != null);


        if (requiredFieldsPresent && startBeforeEnd) {
            TaskRegistry.addTask(new Task(title, priority, description, category, startDate, endDate));

            try {
                switchToPrimary();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            if (!requiredFieldsPresent) {
                alert.setHeaderText("You missed a required field");
            } else {
                alert.setHeaderText("End date can not be before start date");
            }
            alert.show();

        }
    }

    /**
     * This method is used when the "Go back" button is chosen and takes the user back to the first window (main controller).
     */
    @FXML
    void goBack() {
        try {
            switchToPrimary();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
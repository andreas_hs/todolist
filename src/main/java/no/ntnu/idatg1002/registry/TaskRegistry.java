package no.ntnu.idatg1002.registry;

import no.ntnu.idatg1002.fileReadWriteSystem.FileReadWrite;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The apps registry.
 */
public class TaskRegistry {


    /**
     * Sets up a static arrayList for the registry.
     */
    public static List<Task> taskArrayList = new ArrayList<>();

    /**
     * Method to add tasks, takes a task and adds it to the registry and writes it to the file.
     *
     * @param task task to be added.
     */
    public static void addTask(Task task) {
        taskArrayList.add(task);
        FileReadWrite.writeToFile(taskArrayList);

    }

    /**
     * Method that removes a task. Takes in the specific task to be removed.
     *
     * @param taskRemove task object to remove.
     */
    public static void removeSelectedTask(Task taskRemove) {
        taskArrayList.remove(taskRemove);
        FileReadWrite.writeToFile(taskArrayList);
    }

    /**
     * Checks if the registry is empty.
     *
     * @return returns true if the list is empty and false if it isn't.
     */
    public boolean isEmpty() {
        return taskArrayList.isEmpty();
    }

    /**
     * Simple method that reads the file where the registry is saved. Throws either a IO or ClassNotFound exception.
     *
     * @throws IOException can be caught.
     */
    public static void readFromFile() throws IOException {
        taskArrayList = FileReadWrite.readFromFile();
    }

    /**
     * Changes the "doing" status of the task.
     *
     * @param status    which status to be set.
     * @param givenTask task that will get a new status.
     */
    public static void changeDoingStatus(String status, Task givenTask) {
        givenTask.setDoingStatus(status);
        FileReadWrite.writeToFile(taskArrayList);

    }

}

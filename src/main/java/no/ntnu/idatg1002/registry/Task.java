package no.ntnu.idatg1002.registry;

import java.time.LocalDate;

/**
 * Class representing a task. A task contains a title, priority, description, category, start date, end date and doing status.
 */
public class Task {

    /**
     * Sets up the fields for the task.
     */
    private final String title;
    private final String priority;
    private final String description;
    private final String category;
    private final LocalDate startDate;
    private final LocalDate endDate;

    private String doingStatus;


    /**
     * The constructor for the task object.
     *
     * @param title       the title string.
     * @param priority    the priority string.
     * @param description the description string.
     * @param category    the category string.
     * @param startDate   the starDate LocalDate.
     * @param endDate     the description LocalDate.
     */
    public Task(String title, String priority, String description, String category, LocalDate startDate, LocalDate endDate) {
        this.title = title;
        this.priority = priority;
        this.description = description;
        this.category = category;
        this.startDate = startDate;
        this.endDate = endDate;
        doingStatus = "toDo";
    }

    /**
     * Returns the title.
     */
    public String getTitle() {
        return title;
    }


    /**
     * Returns the priority.
     *
     * @return the priority string.
     */
    public String getPriority() {
        return priority;
    }


    /**
     * Returns the description.
     *
     * @return the description string.
     */
    public String getDescription() {
        return description;
    }


    /**
     * Returns the category.
     *
     * @return the category string.
     */
    public String getCategory() {
        return category;
    }


    /**
     * Returns the start date.
     *
     * @return the start date LocalDate.
     */
    public LocalDate getStartDate() {
        return startDate;
    }


    /**
     * Returns the end date.
     *
     * @return the end date LocalDate.
     */
    public LocalDate getEndDate() {
        return endDate;
    }


    /**
     * Returns the doing status.
     *
     * @return the Doing status string.
     */
    public String getDoingStatus() {
        return doingStatus;
    }

    /**
     * Sets the doing status to one of the two possibilities.
     *
     * @param status the doing status string.
     */
    public void setDoingStatus(String status) {
        if (status.equalsIgnoreCase("toDo")) {
            doingStatus = "ToDo";

        } else if (status.equalsIgnoreCase("Done")) {
            doingStatus = "Done";

        }

    }
}

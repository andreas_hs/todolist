package no.ntnu.idatg1002;

import java.io.IOException;
import java.time.LocalDate;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import no.ntnu.idatg1002.registry.Task;
import no.ntnu.idatg1002.registry.TaskRegistry;

/**
 * The primary controller for the app.
 */
public class PrimaryController {

    /**
     * Fields for the main controller.
     */
    @FXML
    public Button exitButton;
    @FXML
    public Task taskToEdit;
    @FXML
    private TableView<Task> taskTableView;
    @FXML
    private TableColumn<Task, String> taskTitle;
    @FXML
    private TableColumn<Task, String> taskDescription;
    @FXML
    private TableColumn<Task, String> taskCategory;
    @FXML
    private TableColumn<Task, String> taskPriority;
    @FXML
    private TableColumn<Task, LocalDate> taskStartDate;
    @FXML
    private TableColumn<Task, LocalDate> taskEndDate;
    @FXML
    private TableColumn<Task, String> doingStatus;

    /**
     * Gets the tasks form the registry and returns them in an Observable arraylist.
     *
     * @return an ObservableList with the tasks from the registry.
     */
    @FXML
    private ObservableList<Task> getTasks() {

        ObservableList<Task> tasksObservableList = FXCollections.observableArrayList();
        tasksObservableList.addAll(TaskRegistry.taskArrayList);

        return tasksObservableList;
    }

    /**
     * This method switches the app to the editTask window.
     *
     * @throws IOException can be caught.
     */
    private void switchToEdit() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("editTask.fxml"));
        Parent root = fxmlLoader.load();
        EditTaskController editTaskController = fxmlLoader.getController();
        editTaskController.setEditTask(taskTableView.getSelectionModel().getSelectedItem());
        App.setRoot(root);
    }

    /**
     * Allows the user to edit the selected task.
     * This method gives the user alerts if there are no tasks to edit or if they haven't selected one.
     */
    @FXML
    public void editTask() {
        if (TaskRegistry.taskArrayList.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("There are no tasks to edit");
            alert.show();
            return;
        }

        taskToEdit = taskTableView.getSelectionModel().getSelectedItem();
        if (taskToEdit != null) {
            try {
                switchToEdit();
            } catch (IOException e) {
                e.printStackTrace();
            }

            TaskRegistry.removeSelectedTask(taskToEdit);
            taskTableView.setItems(getTasks());

        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("You need to select a task to edit");
            alert.show();
        }

    }

    /**
     * This method switches the app to the secondary controller.
     *
     * @throws IOException can be caught.
     */
    @FXML
    private void switchToAddTask() throws IOException {
        App.setRoot("addTask");
    }

    /**
     * This method is used when the user clicks the "Add" button.
     *
     * @throws IOException can be caught.
     */
    @FXML
    void addTask() throws IOException {
        switchToAddTask();
    }

    /**
     * This is the initialize method for the controller. It sets the correct Cell values and gets the tasks from the registry.
     *
     * @throws IOException            can be caught.
     */
    @FXML
    public void initialize() throws IOException {
        TaskRegistry.readFromFile();
        taskTitle.setCellValueFactory(new PropertyValueFactory<>("Title"));
        taskDescription.setCellValueFactory(new PropertyValueFactory<>("Description"));
        taskCategory.setCellValueFactory(new PropertyValueFactory<>("Category"));
        taskPriority.setCellValueFactory(new PropertyValueFactory<>("Priority"));
        taskStartDate.setCellValueFactory(new PropertyValueFactory<>("StartDate"));
        taskEndDate.setCellValueFactory(new PropertyValueFactory<>("EndDate"));
        doingStatus.setCellValueFactory(new PropertyValueFactory<>("doingStatus"));
        taskTableView.refresh();
        taskTableView.setItems(getTasks());


    }

    /**
     * This is the remove method for the controller. It removes the task that is clicked and will give you an alert if you haven't chosen any task or if there are none.
     */
    @FXML
    public void remove() {
        if (TaskRegistry.taskArrayList.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("There is no task to remove");
            alert.show();
            return;
        }

        Task t1 = taskTableView.getSelectionModel().getSelectedItem();
        if (t1 != null) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to remove this task?", ButtonType.YES, ButtonType.NO);
            alert.showAndWait();

            if (alert.getResult() == ButtonType.YES) {
                TaskRegistry.removeSelectedTask(t1);
                taskTableView.setItems(getTasks());
            }

        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("You need to select a task to remove");
            alert.show();
        }

    }

    /**
     * Changes the doing status of the task.
     * If there is no task to change you will receive an alert.
     */
    @FXML
    public void changeDoingStatus() {
        if (TaskRegistry.taskArrayList.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("There is no task to change status of");
            alert.show();
            return;
        }

        Task t1 = taskTableView.getSelectionModel().getSelectedItem();
        if (t1 != null) {
            if (t1.getDoingStatus().equalsIgnoreCase("toDo")) {
                TaskRegistry.changeDoingStatus("done", t1);
            } else {
                TaskRegistry.changeDoingStatus("todo", t1);
            }


            taskTableView.refresh();

        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("You need to select a task to change status!");
            alert.show();
        }

    }

    /**
     * This method exits the app and asks the user to confirm that they want to exit before exiting.
     */
    public void exit() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to exit the program?", ButtonType.YES, ButtonType.NO);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {

            Stage stage = (Stage) exitButton.getScene().getWindow();
            // do what you have to do
            stage.close();
        }
    }

}
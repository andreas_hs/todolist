package no.ntnu.idatg1002.fileReadWriteSystem;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import no.ntnu.idatg1002.registry.Task;

/**
 * Class for writing and reading files.
 */
public class FileReadWrite {
    private FileReadWrite() {

    }

    /**
     * Sets up a Gson builder.
     */
    static Gson gson = new GsonBuilder()
            .setPrettyPrinting()
            .registerTypeAdapter(LocalDate.class, new LocalDateAdapter())
            .create();

    /**
     * This methods takes the arraylist and tries to write it to the json file and throws and Exception if it fails.
     *
     * @param arrayList takes an arrayList that will be written to file.
     */
    public static void writeToFile(List<Task> arrayList) {

        try {

            try (FileOutputStream fileOut = new FileOutputStream("objectFile.json")) {

                String json;
                json = gson.toJson(arrayList);

                fileOut.write(json.getBytes(StandardCharsets.UTF_8));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * This method reads from the file and returns a List with all the tasks found in the file.
     *
     * @return a List with tasks from the file.
     * @throws IOException can be caught.
     */
    public static List<Task> readFromFile() throws IOException {
        if (!new File("objectFile.json").exists()) {
            List a = new ArrayList();
            writeToFile(a);
        }

        Reader reader = Files.newBufferedReader(Paths.get("objectFile.json"));


        return gson.fromJson(reader, new TypeToken<List<Task>>() {
        }.getType());


    }

}
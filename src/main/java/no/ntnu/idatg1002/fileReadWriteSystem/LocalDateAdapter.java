package no.ntnu.idatg1002.fileReadWriteSystem;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Class that can convert LocalDate to String and back.
 */
class LocalDateAdapter implements JsonSerializer<LocalDate>, JsonDeserializer<LocalDate> {

    /**
     * Converts LocalDate to string.
     *
     * @param localDate the Local date object.
     * @param type the Type object
     * @param jsonSerializationContext Json serialization context.
     * @return a new Json primitive object.
     */
    @Override
    public JsonElement serialize(LocalDate localDate, Type type, JsonSerializationContext jsonSerializationContext) {
        return new JsonPrimitive(localDate.format(DateTimeFormatter.ISO_LOCAL_DATE)); // "yyyy-mm-dd"
    }

    /**
     * This converts String to LocalDate.
     *
     * @param jsonElement the Json element object.
     * @param type the Type object
     * @param jsonDeserializationContext Json serialization context.
     * @return the Local date parsed.
     * @throws JsonParseException can be caught.
     */
    @Override
    public LocalDate deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        return LocalDate.parse(jsonElement.getAsString());
    }
}

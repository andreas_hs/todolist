module no.ntnu.idatg2001 {
    requires javafx.controls;
    requires javafx.fxml;
    requires com.google.gson;

    opens no.ntnu.idatg1002 to javafx.fxml, com.google.gson;
    exports no.ntnu.idatg1002;
    exports no.ntnu.idatg1002.fileReadWriteSystem;
    opens no.ntnu.idatg1002.fileReadWriteSystem to com.google.gson, javafx.fxml;
    exports no.ntnu.idatg1002.registry;
    opens no.ntnu.idatg1002.registry to com.google.gson, javafx.fxml;
}


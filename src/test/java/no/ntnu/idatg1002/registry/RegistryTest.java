package no.ntnu.idatg1002.registry;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for the different methods used by the app.
 */
public class RegistryTest
{
    /**
     * Setting up the registry to be used with the tests.
     */
    private Task task1;

    /**
     * This simple method creates the registry to be used with the tests as well as a Task.Then adds the tasks.
     */
    /*
    @BeforeAll
    static void initialize()
     {

     }*/

     @BeforeEach
     void initialize2()
     {
         TaskRegistry.taskArrayList.clear();
         task1 = new Task("Test Oppgave", "High", "Hei, jeg er en kul test!", "Tester og slik", LocalDate.of(2021, 4, 10), LocalDate.of(2021, 5, 6));
         //TaskRegistry.taskArrayList.add(task1);
         TaskRegistry.addTask(task1);
     }

    /**
     * Checks that the title we made actually got added to the registry.
     */
    @Test
    void addTestPositive()
    {
        assertEquals("Test Oppgave",task1.getTitle());
    }

    /**
     * Checks that an incorrect title was not added to the registry.
     */
    @Test
    void addTestNegative()
    {
        assertNotEquals("Ikke riktig tittel!", task1.getTitle());
    }

    /**
     * Checks that the removeTask method actually manages to remove a task.
     */
    @Test
    void removeTestPositive()
    {
        TaskRegistry.removeSelectedTask(task1);
        assertTrue(TaskRegistry.taskArrayList.isEmpty());
    }

    /**
     * This method checks that the isEmpty method returns "False" as the registry is not empty.
     */
    @Test
    void isEmptyTestPositive()
    {
        //initialize();
        assertFalse(TaskRegistry.taskArrayList.isEmpty());
    }

    /**
     * Checks that the app can read from the file without getting any error.
     * This test checks the write method too, as the readFromFile would not be able to retrieve any task if it was not written to the task.
     * @throws IOException can be caught.
     * @throws ClassNotFoundException can be caught.
     */
    @Test
    void readFromFileTest() throws IOException, ClassNotFoundException
    {
        //initialize();

        try
        {
            TaskRegistry.readFromFile();
        } catch (Exception e)
        {
            e.getMessage();
        }

        assertEquals(1,TaskRegistry.taskArrayList.size());
    }

    /**
     * Simple test to check that the changeDoingStatus method manages to change the status of the task.
     */
    @Test
    void changeDoingStatusTest()
    {
        TaskRegistry.changeDoingStatus("Done",task1);
        assertEquals("Done",task1.getDoingStatus());
    }
}
